import org.apache.spark.sql.functions._;

case class worker(id: Int, name: String, sex: String, country: String);

case class workplace (id: Int, department: String);

val workers = sc.textFile("/tests/workers.txt");

val workplaces = sc.textFile("/tests/workplaces.txt");

val workerRDD = workers.map(line=>new worker(line.split(",")(0).toInt,
                                  line.split(",")(1),
                                  line.split(",")(2),
                                  line.split(",")(3)));
val workerDF = workerRDD.toDF();

val workplacesRDD = workplaces.map(line=>new workplace(line.split(",")(0).toInt,
                                                        line.split(",")(1)));

val workplacesDF = workplacesRDD.toDF();

// 1.solution
// val notqaDF = workerDF.join(workplacesDF, Seq("id")).filter(workplacesDF("department")==="qa");
// val notqaDF = workerDF.join(workplacesDF, Seq("id")).filter("department <> 'developer'");
// val notqaDF = workerDF.join(workplacesDF, Seq("id"), "left").filter(workplacesDF("department").isNull);
// val notqaDF = workerDF.join(workplacesDF, Seq("id"), "left").filter("department is null");

val allDF = workerDF.join(workplacesDF, Seq("id")).withColumn("tester", when(workplacesDF("department")==="qa", lit("1")).otherwise(lit("0")));

// 2.solution
// workerDF.registerTempTable("worker");
// workplacesDF.registerTempTable("workplaces");

// val resultDF = sqlContext.sql("select w.id, w.name, w.country, wp.department from worker w LEFT JOIN workplaces wp ON w.id = wp.id");

// 3.solution
// val resultDF = workerDF.join(workplacesDF, workerDF("id")===workplacesDF("id"), "inner").
//                                      select(workerDF("name"), workerDF("country"), workplacesDF("department"));


allDF.show();